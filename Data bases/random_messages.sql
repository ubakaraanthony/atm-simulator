CREATE TABLE `random_messages` (
  `id` int NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `atm_schema`.`random_messages`
(`id`,
`message`)
VALUES
(1,
'A wise person should have money in their head, but not in their heart. --Jonathan Swift');

INSERT INTO `atm_schema`.`random_messages`
(`id`,
`message`)
VALUES
(2,
'Wealth consists not in having great possessions, but in having few wants. --Epictetus');

