CREATE DATABASE `atm_schema` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `account_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `account_number` bigint NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`,`account_number`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `atm_schema`.`account_details` (`id`, `account_number`, `password`) VALUES (1, 986516091798651, '8899');
INSERT INTO `atm_schema`.`account_details` (`id`, `account_number`, `password`) VALUES (2, 861040843086104, '1234');