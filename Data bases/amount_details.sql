CREATE TABLE `amount_details` (
  `account_number` bigint NOT NULL,
  `amount` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `atm_schema`.`amount_details`
(`account_number`,
`amount`)
VALUES
(986516091798651,
120000);
