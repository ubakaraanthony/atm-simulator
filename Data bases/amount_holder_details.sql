CREATE TABLE `account_holder_details` (
  `account_number` bigint NOT NULL,
  `holder_name` varchar(45) NOT NULL,
  `holder_number` varchar(45) NOT NULL,
  `aadhar_number` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`account_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `atm_schema`.`account_holder_details`
(`account_number`,
`holder_name`,
`holder_number`,
`aadhar_number`)
VALUES
(986516091798651,
'Ubakara Anthony F',
'9865160917',
'123412341234');
