package deposit;

import dbConnection.AtmCrudOperation;
import design.BeanClass;

public class DepositeServiceImpl implements DepositService{
	private long depositedAmount;
	public BeanClass beanClass;
	
	public boolean deposit() {
		AtmCrudOperation atmCrudOperation =AtmCrudOperation.getInstance();
		beanClass = BeanClass.getObject();
		long holdAmount = beanClass.getAmount();
		long depositAmount = beanClass.getDepositAmount();
		depositedAmount = holdAmount + depositAmount;
		boolean depositCheck = atmCrudOperation.depositamount(depositedAmount);
		return depositCheck;
	}
}
