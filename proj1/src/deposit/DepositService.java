package deposit;

@FunctionalInterface
public interface DepositService {
	public boolean deposit();
}
