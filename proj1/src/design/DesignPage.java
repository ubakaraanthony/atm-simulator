package design;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import dbConnection.AtmCrudOperation;
import deposit.DepositService;
import deposit.DepositeServiceImpl;
import transfer.TransferServiceImpl;
import withdraw.WithdrawServiceImpl;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class DesignPage implements ActionListener, FocusListener {

	public JFrame frame,frame2;
	public JTextField accountNumber,amount, transferAccountNumber, amountTxt;
	public JButton enter, clear, deposit,transfer, miniStatement, withdraw, 
					balanceEnq, viewProfile, cancel, delete, one, two, three, 
						four, five, six, seven, eight, nine, zero, home;
	public JPanel panel_2;
	public JLabel passMsg, accNumber, passlbl, welcomelbl, msgLbl, getAmountLabel, depositeMsg, 
						thankYouLbl, getAmountLabelWithdraw, withdrawMsg, holdingAmountLabel, amountLabel, 
						accountNumberlbl, holderName, contactNumber, AadharNumber,accountNumberTxt, holderNameTxt, 
						contactNumberTxt, AadharNumberTxt, transferAccountNumberlbl, amountlbl, transfermsg;
	public JPasswordField passwordField;
	private JTextField previouslyFocused = accountNumber;
	public Panel panel_3; 
	DepositeServiceImpl depo;
	public BeanClass beanClass;
	boolean enterStatus = false;
	boolean depositStatus = false;
	boolean withdrawStatus = false;
	boolean balanceEnqStatus = false;
	boolean viewProfileStatus = false;
	boolean transferStatus = false;
	boolean cancelStatus = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DesignPage window = new DesignPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DesignPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 1280, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 885, 639);
		panel.setBackground(UIManager.getColor("Button.light"));
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(83, 41, 718, 567);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		panel_3 = new Panel();
		panel_3.setBackground(Color.LIGHT_GRAY);
		panel_3.setBounds(193, 60, 332, 451);
		panel_2.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel welcomeLabel = new JLabel("<html>Welcome To ABCD Bank<html>");
		welcomeLabel.setBounds(71, 5, 300, 19);
		welcomeLabel.setFont(new Font("Arial", Font.BOLD, 16));
		panel_3.add(welcomeLabel);
		
		accNumber = new JLabel("Account Number: ");
		accNumber.setFont(new Font("Arial", Font.BOLD, 12));
		accNumber.setBounds(10, 58, 107, 14);
		panel_3.add(accNumber);
		
		accountNumber = new JTextField();
		accountNumber.setBackground(new Color(192, 192, 192));
		accountNumber.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		accountNumber.setBounds(127, 55, 195, 20);
		panel_3.add(accountNumber);
		accountNumber.addFocusListener(this);
		accountNumber.setColumns(10);
		
		passMsg = new JLabel("");
		passMsg.setVerticalAlignment(SwingConstants.TOP);
		passMsg.setForeground(Color.RED);
		passMsg.setBounds(127, 146, 195, 50);
		panel_3.add(passMsg);
		
		passlbl = new JLabel("Password:");
		passlbl.setFont(new Font("Arial", Font.BOLD, 12));
		passlbl.setBounds(48, 124, 69, 14);
		panel_3.add(passlbl);
		
		passwordField = new JPasswordField();
		passwordField.setBackground(Color.LIGHT_GRAY);
		passwordField.setBounds(127, 121, 195, 20);
		passwordField.addFocusListener(this);
		panel_3.add(passwordField);
		
		deposit = new JButton("DEPOSIT");  //over
		deposit.setEnabled(false);
		deposit.setFont(new Font("Arial", Font.BOLD, 12));
		deposit.setBackground(new Color(192, 192, 192));
		deposit.setBounds(10, 60, 162, 47);
		deposit.addActionListener(this);
		panel_2.add(deposit);
		
		transfer = new JButton("TRANSFER"); //in progress
		transfer.setEnabled(false);
		transfer.setFont(new Font("Arial", Font.BOLD, 12));
		transfer.setBackground(new Color(192, 192, 192));
		transfer.setBounds(10, 210, 162, 47);
		transfer.addActionListener(this);
		panel_2.add(transfer);
		
		miniStatement = new JButton("MINI STATEMENT");
		miniStatement.setFont(new Font("Arial", Font.BOLD, 12));
		miniStatement.setBackground(new Color(192, 192, 192));
		miniStatement.setEnabled(false);
		miniStatement.setBounds(10, 360, 162, 47);
		//miniStatement.addActionListener(this);
		panel_2.add(miniStatement);
		
		withdraw = new JButton("WITHDRAWAL");  //Complete
		withdraw.setEnabled(false);
		withdraw.setFont(new Font("Arial", Font.BOLD, 12));
		withdraw.setBackground(new Color(192, 192, 192));
		withdraw.setBounds(546, 60, 162, 47);
		withdraw.addActionListener(this);
		panel_2.add(withdraw);
		
		balanceEnq = new JButton("BALANCE ENQUIRY"); //Complete
		balanceEnq.setEnabled(false);
		balanceEnq.setFont(new Font("Arial", Font.BOLD, 12));
		balanceEnq.setBackground(new Color(192, 192, 192));
		balanceEnq.setBounds(546, 210, 162, 47);
		balanceEnq.addActionListener(this);
		panel_2.add(balanceEnq);
		
		viewProfile = new JButton("VIEW PROFILE"); //Complete
		viewProfile.setEnabled(false);
		viewProfile.setFont(new Font("Arial", Font.BOLD, 12));
		viewProfile.setBackground(new Color(192, 192, 192));
		viewProfile.setBounds(546, 360, 162, 47);
		viewProfile.addActionListener(this);
		panel_2.add(viewProfile);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(UIManager.getColor("Button.shadow"));
		panel_1.setBounds(905, 110, 349, 424);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(4, 4, 0, 0));
		
		one = new JButton("1");
		one.setFont(new Font("Arial", Font.BOLD, 12));
		one.setBackground(new Color(192, 192, 192));
		panel_1.add(one);
		one.addActionListener(this);
		
		two = new JButton("2");
		two.setFont(new Font("Arial", Font.BOLD, 12));
		two.setBackground(new Color(192, 192, 192));
		panel_1.add(two);
		two.addActionListener(this);
		
		three = new JButton("3");
		three.setFont(new Font("Arial", Font.BOLD, 12));
		three.setBackground(new Color(192, 192, 192));
		panel_1.add(three);
		three.addActionListener(this);
		
		cancel = new JButton("Cancel");
		cancel.setEnabled(false);
		cancel.setFont(new Font("Arial", Font.BOLD, 12));
		cancel.setBackground(new Color(192, 192, 192));
		panel_1.add(cancel);
		cancel.addActionListener(this);
		
		four = new JButton("4");
		four.setFont(new Font("Arial", Font.BOLD, 12));
		four.setBackground(new Color(192, 192, 192));
		panel_1.add(four);
		four.addActionListener(this);
		
		five = new JButton("5");
		five.setFont(new Font("Arial", Font.BOLD, 12));
		five.setBackground(new Color(192, 192, 192));
		panel_1.add(five);
		five.addActionListener(this);
		
		six = new JButton("6");
		six.setFont(new Font("Arial", Font.BOLD, 12));
		six.setBackground(new Color(192, 192, 192));
		panel_1.add(six);
		six.addActionListener(this);
		
		delete = new JButton("Delete");
		delete.setEnabled(false);
		delete.setFont(new Font("Arial", Font.BOLD, 12));
		delete.setBackground(new Color(192, 192, 192));
		panel_1.add(delete);
		delete.addActionListener(this);
		
		seven = new JButton("7");
		seven.setFont(new Font("Arial", Font.BOLD, 12));
		seven.setBackground(new Color(192, 192, 192));
		panel_1.add(seven);
		seven.addActionListener(this);
		
		eight = new JButton("8");
		eight.setFont(new Font("Arial", Font.BOLD, 12));
		eight.setBackground(new Color(192, 192, 192));
		panel_1.add(eight);
		eight.addActionListener(this);
		
		nine = new JButton("9");
		nine.setFont(new Font("Arial", Font.BOLD, 12));
		nine.setBackground(new Color(192, 192, 192));
		panel_1.add(nine);
		nine.addActionListener(this);
		
		enter = new JButton("Enter");
		enter.setEnabled(false);
		enter.setFont(new Font("Arial", Font.BOLD, 12));
		enter.setBackground(new Color(192, 192, 192));
		panel_1.add(enter);
		enter.addActionListener(this);
		
		JButton btnNewButton_11 = new JButton("");
		btnNewButton_11.setBackground(new Color(192, 192, 192));
		panel_1.add(btnNewButton_11);
		
		zero = new JButton("0");
		zero.setFont(new Font("Arial", Font.BOLD, 12));
		zero.setBackground(new Color(192, 192, 192));
		panel_1.add(zero);
		zero.addActionListener(this);
		
		JButton btnNewButton_14 = new JButton("");
		btnNewButton_14.setBackground(new Color(192, 192, 192));
		panel_1.add(btnNewButton_14);
		
		clear = new JButton("Clear");
		clear.setEnabled(false);
		clear.setFont(new Font("Arial", Font.BOLD, 12));
		clear.setBackground(new Color(192, 192, 192));
		panel_1.add(clear);
		clear.addActionListener(this);
		
		home = new JButton("HOME");
		home.setFont(new Font("Arial", Font.BOLD, 12));
		home.setBackground(Color.green);
		home.setBounds(85, 330, 162, 47);
		home.setVisible(false);
		home.addActionListener(this);
		panel_3.add(home);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==one) {
			accountNumberText("1");
		}
		if(e.getSource()==two) {
			accountNumberText("2");
		}
		if(e.getSource()==three) {
			accountNumberText("3");
		}
		if(e.getSource()==four) {
			accountNumberText("4");
		}
		if(e.getSource()==five) {
			accountNumberText("5");
		}
		if(e.getSource()==six) {
			accountNumberText("6");
		}
		if(e.getSource()==seven) {
			accountNumberText("7");
		}
		if(e.getSource()==eight) {
			accountNumberText("8");
		}
		if(e.getSource()==nine) {
			accountNumberText("9");
		}
		if(e.getSource()==zero) {
			accountNumberText("0");
		}
		if(e.getSource()==clear) {
			passMsg.setText("");
			accountNumber.setText("");
			passwordField.setText("");
			if(depositStatus==true || withdrawStatus==true) {
			amount.setText("");
			depositMsgDisable();
			}
			if(transferStatus==true) {
				amountTxt.setText("");
				transferAccountNumber.setText("");
			}
			if(enterStatus==false) {
				clear.setEnabled(false);
				enter.setEnabled(false);
				delete.setEnabled(false);
			}
		}
		if(e.getSource()==cancel) {
			if(cancelStatus==false && depositStatus==true || withdrawStatus==true || transferStatus==true || viewProfileStatus==true || balanceEnqStatus==true) {
			cancelStatus=true;
			enter.setEnabled(true);
			printThankyou();
			}
		}
		
		if(e.getSource()==delete) {
			int len = previouslyFocused.getText().length();
			int num = previouslyFocused.getText().length()-1;
			String store;
			StringBuilder str = new StringBuilder(previouslyFocused.getText());
			if(len>0) {
				str.deleteCharAt(num);
				store = str.toString();
				previouslyFocused.setText(store);
			}
			if(depositStatus==true || withdrawStatus==true || transferStatus==true) {
				depositMsgDisable();
			}
		}
		
		if(e.getSource()==enter) {
			if(enterStatus==false) {
				enterOperationOnly();
			}
			else if(depositStatus==true && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
				if(!(amount.getText().isBlank()) && Long.parseLong(amount.getText())>0 && Long.parseLong(amount.getText())%100==0 && Long.parseLong(amount.getText())<20000) {
				enterForDepositOperation();
				}
				else if(amount.isVisible()) {
					depositeMsg.setVisible(true);
					depositeMsg.setText("<html>Note*:<br>1. Multiples of Hundred <br/>2. Amount should be greater than 0 <br/>3. less than 20000</html>");
				}
			}
			else if(withdrawStatus==true && depositStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
				if(!(amount.getText().isBlank()) && Long.parseLong(amount.getText())>0 && Long.parseLong(amount.getText())%100==0 && Long.parseLong(amount.getText())<20000) {
						enterForwithdrawOperation();
					}
				else if(amount.isVisible()) {
					depositeMsg.setVisible(true);
					depositeMsg.setText("<html>Note*:<br>1. Multiples of Hundred <br/>2. Amount should be greater than 0 <br/>3. less than 20000</html>");
				}
			}
			else if(withdrawStatus==false && depositStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==true) {
				if(!(transferAccountNumber.getText().isBlank()) && transferAccountNumber.getText().length()<=15) {
				if(!(amountTxt.getText().isBlank()) && Long.parseLong(amountTxt.getText())>0 && Long.parseLong(amountTxt.getText())%100==0 && Long.parseLong(amountTxt.getText())<15000) {
					enterForTransferOperation();
				}
				}
				else if(amountTxt.isVisible()) {
					depositeMsg.setVisible(true);
					depositeMsg.setText("<html>Note*:<br>1. Multiples of Hundred <br/>2. Amount should be greater than 0 <br/>3. less than 15000</html>");
				}
			}
		}
		
		if(e.getSource()==deposit && depositStatus==false && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
			deposit.setBackground(Color.GRAY);
			depositDesign();
		}
		
		if(e.getSource()==home) {
			afterLogin();
			if(cancelStatus==true) {
				cancelStatus=false;
			}
			
			if(accountNumberlbl!=null) {
				if(accountNumberlbl.isVisible()) {
					accountNumberlbl.setVisible(false);
				}
				if(accountNumberTxt.isVisible()) {
					accountNumberTxt.setVisible(false);
				}
				if(holderName.isVisible()) {
					holderName.setVisible(false);
				}
				if(holderNameTxt.isVisible()) {
					holderNameTxt.setVisible(false);
				}
				if(contactNumber.isVisible()) {
					contactNumber.setVisible(false);
				}
				if(contactNumberTxt.isVisible()) {
					contactNumberTxt.setVisible(false);
				}
				if(AadharNumber.isVisible()) {
					AadharNumber.setVisible(false);
				}
				if(AadharNumberTxt.isVisible()) {
					AadharNumberTxt.setVisible(false);
				}
				
				
			}
			
			if(getAmountLabel!=null) {
				if(getAmountLabel.isVisible()) { 
				getAmountLabel.setVisible(false);
				}
			}
			if(withdrawStatus==true) {
				withdrawStatus=false;
				withdraw.setBackground(Color.LIGHT_GRAY);
				}
			if(depositStatus == true) {
				depositStatus = false;
				deposit.setBackground(Color.LIGHT_GRAY);
				}
			if(balanceEnqStatus == true) {
				balanceEnqStatus = false;
				balanceEnq.setBackground(Color.LIGHT_GRAY);
				}
			if(viewProfileStatus==true) {
				viewProfileStatus=false;
				viewProfile.setBackground(Color.LIGHT_GRAY);
			}
			if(transferStatus==true) {
				transferStatus=false;
				transfer.setBackground(Color.LIGHT_GRAY);
			}
		}
		
		if(e.getSource()==withdraw && depositStatus==false && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
			withdraw.setBackground(Color.GRAY);
			withdrawDesign();
		}
		
		if(e.getSource()==balanceEnq && depositStatus==false && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
			balanceEnq.setBackground(Color.gray);
			BalanceEnquiryDesign();
		}
		
		if(e.getSource()==viewProfile && depositStatus==false && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
			viewProfile.setBackground(Color.gray);
			viewProfileDesign();
		}
		if(e.getSource()==transfer && depositStatus==false && withdrawStatus==false && balanceEnqStatus==false && viewProfileStatus==false && transferStatus==false) {
			transfer.setBackground(Color.gray);
			enter.setEnabled(false);
			transferDesign();
		}
	}
	

	private void enterForTransferOperation() {
		beanClass.setTransferAccountNumber(Long.parseLong(transferAccountNumber.getText()));
		beanClass.setTransferAmount(Long.parseLong(amountTxt.getText()));
		TransferServiceImpl transferserv = new TransferServiceImpl();
		try {
		boolean transferCheck = transferserv.transfer();
		if(transferCheck==true) {
			amountTxt.setVisible(false);
			transferAccountNumber.setVisible(false);
			transferAccountNumberlbl.setVisible(false);
			amountlbl.setVisible(false);
			getAmountLabel.setText("Amount Successfully Transfered!!");
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000 
			);
		}
		else {
			amountTxt.setVisible(false);
			transferAccountNumber.setVisible(false);
			transferAccountNumberlbl.setVisible(false);
			amountlbl.setVisible(false);
			getAmountLabel.setText("<html>Insufficient Amount...<br>Transfered Failed:(</html>");
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000 
			);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	private void transferDesign() {
		panel_3.setBackground(Color.ORANGE);
		transferStatus=true;
		if(msgLbl!=null) {
			if(msgLbl.isVisible()) {
					msgLbl.setVisible(false);
			}
		}
		beanClass = BeanClass.getObject();
		transferAccountNumberlbl = new JLabel();
		transferAccountNumberlbl.setVisible(true);
		transferAccountNumberlbl.setText("Account Number: ");
		transferAccountNumberlbl.setFont(new Font("Arial", Font.BOLD, 12));
		transferAccountNumberlbl.setBounds(10, 95, 300, 30);
		panel_3.add(transferAccountNumberlbl);
		
		transferAccountNumber = new JTextField();
		transferAccountNumber.setVisible(true);
		transferAccountNumber.setBackground(new Color(192, 192, 192));
		transferAccountNumber.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		transferAccountNumber.setBounds(120, 100, 195, 20);
		transferAccountNumber.addFocusListener(this);
		panel_3.add(transferAccountNumber);
		
		amountlbl = new JLabel();
		amountlbl.setVisible(true);
		amountlbl.setText("Amount: ");
		amountlbl.setFont(new Font("Arial", Font.BOLD, 12));
		amountlbl.setBounds(61, 125, 300, 30);
		panel_3.add(amountlbl);
		
		amountTxt = new JTextField();
		amountTxt.setVisible(true);
		amountTxt.setBackground(new Color(192, 192, 192));
		amountTxt.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		amountTxt.setBounds(120, 130, 195, 20);
		amountTxt.addFocusListener(this);
		amountTxt.requestFocus();
		panel_3.add(amountTxt);
		
		depositeMsg = new JLabel();
		depositeMsg.setText("");
		depositeMsg.setFont(new Font("Arial", Font.BOLD, 12));
		depositeMsg.setForeground(Color.RED);
		depositeMsg.setBounds(10, 60, 350, 300);
		depositeMsg.setVisible(false);
		panel_3.add(depositeMsg);
		
		getAmountLabel = new JLabel();
		getAmountLabel.setText("");
		getAmountLabel.setFont(new Font("Arial", Font.BOLD, 16));
		getAmountLabel.setBounds(10, 100, 300, 30);
		getAmountLabel.setVisible(true);
		panel_3.add(getAmountLabel);
		
	}

	private void viewProfileDesign() {
		
		viewProfileStatus=true;
		welcomelbl.setVisible(false);
		if(msgLbl!=null) {
			if(msgLbl.isVisible()) {
					msgLbl.setVisible(false);
			}
		}
		beanClass = BeanClass.getObject();
		panel_3.setBackground(Color.MAGENTA);
		accountNumberlbl = new JLabel();
		accountNumberlbl.setVisible(true);
		accountNumberlbl.setText("Account Number: ");
		accountNumberlbl.setFont(new Font("Arial", Font.BOLD, 16));
		accountNumberlbl.setBounds(10, 100, 300, 30);
		panel_3.add(accountNumberlbl);
		
		accountNumberTxt = new JLabel();
		accountNumberTxt.setVisible(true);
		accountNumberTxt.setText(String.valueOf(beanClass.getAccountNumber()));
		accountNumberTxt.setFont(new Font("Arial", Font.BOLD, 16));
		accountNumberTxt.setBounds(155, 100, 300, 30);
		accountNumberTxt.setForeground(Color.RED);
		panel_3.add(accountNumberTxt);
		
		holderName = new JLabel();
		holderName.setVisible(true);
		holderName.setText("Holder Name: ");
		holderName.setFont(new Font("Arial", Font.BOLD, 16));
		holderName.setBounds(40, 130, 300, 30);
		panel_3.add(holderName);
		
		holderNameTxt = new JLabel();
		holderNameTxt.setVisible(true);
		holderNameTxt.setText(beanClass.getHolderName());
		holderNameTxt.setFont(new Font("Arial", Font.BOLD, 16));
		holderNameTxt.setBounds(155, 130, 300, 30);
		holderNameTxt.setForeground(Color.RED);
		panel_3.add(holderNameTxt);
		
		contactNumber = new JLabel();
		contactNumber.setVisible(true);
		contactNumber.setText("Contact Number: ");
		contactNumber.setFont(new Font("Arial", Font.BOLD, 16));
		contactNumber.setBounds(14, 160, 300, 30);
		panel_3.add(contactNumber);
		
		contactNumberTxt = new JLabel();
		contactNumberTxt.setVisible(true);
		contactNumberTxt.setText(String.valueOf(beanClass.getHolderNumber()));
		contactNumberTxt.setFont(new Font("Arial", Font.BOLD, 16));
		contactNumberTxt.setBounds(155, 160, 300, 30);
		contactNumberTxt.setForeground(Color.RED);
		panel_3.add(contactNumberTxt);
		
		AadharNumber = new JLabel();
		AadharNumber.setVisible(true);
		AadharNumber.setText("Aadhar Number: ");
		AadharNumber.setFont(new Font("Arial", Font.BOLD, 16));
		AadharNumber.setBounds(18, 190, 300, 30);
		panel_3.add(AadharNumber);
		
		AadharNumberTxt = new JLabel();
		AadharNumberTxt.setVisible(true);
		AadharNumberTxt.setText(beanClass.getAadharNumber());
		AadharNumberTxt.setFont(new Font("Arial", Font.BOLD, 16));
		AadharNumberTxt.setBounds(155, 190, 300, 30);
		AadharNumberTxt.setForeground(Color.RED);
		panel_3.add(AadharNumberTxt);
		
		home.setVisible(true);
		
	}

	private void BalanceEnquiryDesign() {
		beanClass = BeanClass.getObject();
		balanceEnqStatus = true;
		holdingAmountLabel = new JLabel();
		holdingAmountLabel.setVisible(true);
		holdingAmountLabel.setText("Your Holding Amount is: ");
		holdingAmountLabel.setFont(new Font("Arial", Font.BOLD, 16));
		holdingAmountLabel.setBounds(10, 100, 300, 30);
		panel_3.setBackground(Color.green);
		panel_3.add(holdingAmountLabel);
		
		if(msgLbl!=null) {
			if(msgLbl.isVisible()) {
					msgLbl.setVisible(false);
			}
		}
		
		amountLabel = new JLabel();
		amountLabel.setVisible(true);
		amountLabel.setText(String.valueOf(beanClass.getAmount()));
		amountLabel.setFont(new Font("Arial", Font.BOLD, 16));
		amountLabel.setBounds(207, 100, 300, 30);
		amountLabel.setForeground(Color.RED);
		panel_3.setBackground(Color.green);
		panel_3.add(amountLabel);
		home.setVisible(true);
	}

	private void enterForwithdrawOperation() {
		beanClass.setWithdrawAmount(Long.parseLong(amount.getText()));
		WithdrawServiceImpl withdrawfunc = new WithdrawServiceImpl();
		try {
		boolean withdrawCheck = withdrawfunc.withDraw();
		if(withdrawCheck==true) {
			getAmountLabel.setText("Account Successfully Withdrawn!!");
			amount.setVisible(false);
			depositeMsg.setVisible(false);
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000 
			);
		}
		else {
			amount.setVisible(false);
			depositeMsg.setVisible(false);
			getAmountLabel.setText("<html>Insufficient Amount...<br>Payment Declined:(</html>");
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000
			);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	private void withdrawDesign() {

		getAmountLabel = new JLabel();
		getAmountLabel.setVisible(true);
		getAmountLabel.setText("Enter the amount to WithDraw...");
		getAmountLabel.setFont(new Font("Arial", Font.BOLD, 16));
		getAmountLabel.setBounds(10, 110, 300, 30);
		panel_3.setBackground(Color.pink);
		panel_3.add(getAmountLabel);
		
		amount = new JTextField();
		amount.setBackground(new Color(192, 192, 192));
		amount.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		amount.setBounds(10, 150, 150, 30);
		panel_3.add(amount);
		amount.setVisible(true);
		amount.addFocusListener(this);
		amount.requestFocus();
		amount.setColumns(10);
		
		depositeMsg = new JLabel();
		depositeMsg.setText("");
		depositeMsg.setFont(new Font("Arial", Font.BOLD, 12));
		depositeMsg.setForeground(Color.RED);
		depositeMsg.setBounds(10, 60, 350, 300);
		depositeMsg.setVisible(false);
		panel_3.add(depositeMsg);
		
		if(msgLbl!=null) {
			if(msgLbl.isVisible()) {
					msgLbl.setVisible(false);
			}
		}
		
		withdrawStatus = true;
		
	}

	protected void depositDesign() {
		if(msgLbl!=null) {
			if(msgLbl.isVisible()) {
					msgLbl.setVisible(false);
			}
		}
		getAmountLabel = new JLabel();
		getAmountLabel.setText("Enter the amount to Deposit...");
		getAmountLabel.setFont(new Font("Arial", Font.BOLD, 16));
		getAmountLabel.setBounds(10, 100, 300, 30);
		getAmountLabel.setVisible(true);
		panel_3.setBackground(Color.CYAN);
		panel_3.add(getAmountLabel);
		
		amount = new JTextField();
		amount.setBackground(new Color(192, 192, 192));
		amount.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 12));
		amount.setBounds(10, 150, 150, 30);
		panel_3.add(amount);
		amount.setVisible(true);
		amount.requestFocus();
		amount.addFocusListener(this);
		amount.setColumns(10);
		
		depositeMsg = new JLabel();
		depositeMsg.setText("");
		depositeMsg.setFont(new Font("Arial", Font.BOLD, 12));
		depositeMsg.setForeground(Color.RED);
		depositeMsg.setBounds(10, 60, 350, 300);
		depositeMsg.setVisible(false);
		panel_3.add(depositeMsg);
		depositStatus = true;		
	}

	protected void depositMsgDisable() {
		if(amount != null) {
		if(amount.getText().length()==0) {
			depositeMsg.setVisible(false);
			}
		}
		if(amountTxt != null) {
		if(amountTxt.isVisible()==true) {
			if(amountTxt.getText().length()==0) {
				depositeMsg.setVisible(false);
			}
		}
		}
		
	}

	protected void enterForDepositOperation() {
		beanClass.setDepositAmount(Long.parseLong(amount.getText()));
		DepositeServiceImpl depositfunc = new DepositeServiceImpl();
		try {
		boolean depositCheck = depositfunc.deposit();
		if(depositCheck==true) {
			getAmountLabel.setText("Account Successfully Deposited!!");
			amount.setVisible(false);
			depositeMsg.setVisible(false);
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000 
			);
		}
		else {
			getAmountLabel.setText("Operation Failed Amount is not deposited to your account");
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	printThankyou();
			            }
			        }, 
			        2000 
			);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	protected void printThankyou() {
		if(getAmountLabel!=null) {
			if(getAmountLabel.isVisible()) {
				getAmountLabel.setText("Thank You!!");
			}
		}
		if(cancelStatus==true) {
		if(withdrawStatus==true || depositStatus==true) {
			if(amount.isVisible()) {
				amount.setVisible(false);
				depositeMsg.setVisible(false);
			}
		}
		if(transferStatus==true) {
			amountTxt.setVisible(false);
			transferAccountNumber.setVisible(false);
			transferAccountNumberlbl.setVisible(false);
			amountlbl.setVisible(false);
			depositeMsg.setVisible(false);
		}
		}
		if(cancelStatus==false) {
				AtmCrudOperation accountExists = AtmCrudOperation.getInstance();
				accountExists.setDataFromAmountDetails();
		}
				home.setVisible(true);
}

	protected void enterOperationOnly() {
		beanClass = BeanClass.getObject();
		long acc = Long.parseLong(accountNumber.getText());
		String pass = passwordField.getText();
		beanClass.setAccountNumber(acc);
		beanClass.setPassword(pass);
		
		AtmCrudOperation accountExists =AtmCrudOperation.getInstance();
		accountExists.setDataFromRandomMessages();
		int status = 0;
		try {
			status = accountExists.accountNumberExists();
			if(status==1) {
				passMsg.setText("");
				deposit.setEnabled(true);
				withdraw.setEnabled(true);
				balanceEnq.setEnabled(true);
				viewProfile.setEnabled(true);
				transfer.setEnabled(true);
				cancel.setEnabled(true);
				accountNumber.setVisible(false);
				passwordField.setVisible(false);
				passMsg.setVisible(false);
				accNumber.setVisible(false);
				passlbl.setVisible(false);
				afterLogin();
			}
			else {
				passMsg.setText("Account Number and Password Incorrect!!!");
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		enterStatus = true;
		
	}

	protected void afterLogin() {
		welcomelbl = new JLabel("Welcome "+beanClass.getHolderName());
		welcomelbl.setFont(new Font("Arial", Font.BOLD, 16));
		welcomelbl.setBounds(10, 58, 300, 30);
		panel_3.setBackground(Color.PINK);
		panel_3.add(welcomelbl);
		
		msgLbl = new JLabel();
		msgLbl.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 20));
		msgLbl.setBounds(48, 150, 250, 200);
		msgLbl.setText("<html>"+beanClass.getMessage()+"</html>");
		msgLbl.setPreferredSize(new Dimension(300, 500));
		panel_3.add(msgLbl);
		
		if(home.isVisible()) {
			home.setVisible(false);
		}
		if(holdingAmountLabel!=null) {
		if(holdingAmountLabel.isVisible() && amountLabel.isVisible()) {
			holdingAmountLabel.setVisible(false);
			amountLabel.setVisible(false);
		}
		}
	}

	protected void accountNumberText(String num) {
		if(previouslyFocused.equals(accountNumber)) {
			accountNumber.setText(accountNumber.getText()+num);
			if(accountNumber.getText().length()>=15) {
				passwordField.requestFocus();
			}
		}
		else if (previouslyFocused.equals(passwordField)) {
			if(passwordField.getText().length()!=4) {
			passwordField.setText(passwordField.getText()+num);
			//accountMsg.setText(passwordField.getText());
			}
			if(passwordField.getText().length()==4 && accountNumber.getText().length()==15) {
				enter.setEnabled(true);
			}
		}
		else if (previouslyFocused.equals(amount)) {
			amount.setText(amount.getText()+num);
		}
		if(accountNumber.getText().length()>0 || passwordField.getText().length()>0) {
			clear.setEnabled(true);
			delete.setEnabled(true);
		}
		if(previouslyFocused.equals(transferAccountNumber)) {
			transferAccountNumber.setText(transferAccountNumber.getText()+num);
			if(transferAccountNumber.getText().length()>=15) {
				amountTxt.requestFocus();
				enter.setEnabled(true);
			}
		}
		if(previouslyFocused.equals(amountTxt)) {
			amountTxt.setText(amountTxt.getText()+num);
		}
	}

	@Override
	public void focusGained(FocusEvent fc) {
		if(fc.getSource() instanceof JTextField) {
			previouslyFocused = (JTextField) fc.getSource();
			System.out.println(previouslyFocused);
		}
		
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
