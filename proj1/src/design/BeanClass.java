package design;


public class BeanClass{
	private static BeanClass beanClass; 
	private long accountNumber;
	private String password;
	private String holderName;
	private String holderNumber;
	private String aadharNumber;
	private long amount;
	private long depositAmount;
	private String message;
	private long withdrawAmount;
	private long transferAccountNumber;
	private long transferAmount;
	
	private BeanClass() {
		
	}
	
	public static BeanClass getObject() {
		if(beanClass==null) {
			beanClass = new BeanClass();
		}
		return beanClass;
	}
	
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getHolderNumber() {
		return holderNumber;
	}

	public void setHolderNumber(String holderNumber) {
		this.holderNumber = holderNumber;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(long depositAmount) {
		this.depositAmount = depositAmount;
	}

	public long getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(long withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public long getTransferAccountNumber() {
		return transferAccountNumber;
	}

	public void setTransferAccountNumber(long transferAccountNumber) {
		this.transferAccountNumber = transferAccountNumber;
	}

	public long getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(long transferAmount) {
		this.transferAmount = transferAmount;
	}
	
}
