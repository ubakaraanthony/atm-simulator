package withdraw;

import dbConnection.AtmCrudOperation;
import design.BeanClass;

public class WithdrawServiceImpl implements WithdrawService {

	private long withdrawAmount;
	public BeanClass beanClass;

	@Override
	public boolean withDraw() {
		AtmCrudOperation atmCrudOperation =AtmCrudOperation.getInstance();
		beanClass = BeanClass.getObject();
		long holdAmount = beanClass.getAmount();
		long withdraw = beanClass.getWithdrawAmount();
		if(holdAmount >= withdraw) {
		withdrawAmount = holdAmount - withdraw;
		boolean depositCheck = atmCrudOperation.depositamount(withdrawAmount);
		return depositCheck;
		}
		return false;
	}
}
