package transfer;

import dbConnection.AtmCrudOperation;
import design.BeanClass;

public class TransferServiceImpl implements TransferService {

	private long yourHoldingAmount;
	private long transferAccountNumber;
	private long transferAmount;
	private Long transferHoldingAmount;
	public BeanClass beanClass;
	@Override
	public boolean transfer() {
		beanClass = BeanClass.getObject();
		transferAmount = beanClass.getTransferAmount();
		transferAccountNumber = beanClass.getTransferAccountNumber();
		yourHoldingAmount = beanClass.getAmount();
		yourHoldingAmount = yourHoldingAmount - transferAmount;
		System.out.println("Holding Amount: "+yourHoldingAmount);
		System.out.println("Transfer Amount: "+transferAmount);
		AtmCrudOperation atmCrudOperation =AtmCrudOperation.getInstance();
		transferHoldingAmount = atmCrudOperation.getAmountByAccountNumber(transferAccountNumber);

		if(transferHoldingAmount!=null) {
		transferHoldingAmount = transferAmount + transferHoldingAmount;
		boolean amountMinusCheck = atmCrudOperation.depositamount(yourHoldingAmount);
		if(amountMinusCheck==true) {
		boolean transferCheck = atmCrudOperation.transferOperation(transferAccountNumber, transferHoldingAmount);
		return transferCheck;
		}
		}
		return false;
		
}
}
