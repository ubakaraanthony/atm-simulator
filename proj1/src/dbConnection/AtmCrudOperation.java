package dbConnection;

import java.sql.*;

import design.BeanClass;

public class AtmCrudOperation extends DbConfiguration {
	
	public BeanClass beanClass = BeanClass.getObject();;
	public static AtmCrudOperation atmCrudOperation;
	private AtmCrudOperation() {
		
	}
	public static AtmCrudOperation getInstance() {
		if(atmCrudOperation==null) {
			atmCrudOperation = new AtmCrudOperation();
		}
		return atmCrudOperation;
	}
	
public int accountNumberExists() throws Exception {
		
	try {
		Class.forName(driver_class);
		con = DriverManager.getConnection(db_url, user, pass);
		System.out.print("DB Connected");
		ps = con.prepareStatement("select * from account_details where account_number=? && password=?");
		ps.setLong(1, beanClass.getAccountNumber());
		ps.setString(2, beanClass.getPassword());
		rs = ps.executeQuery();
		if(rs.next()) {
			setDataFromAccountHolderDetails();
			return 1;
		}
		
	}catch(Exception e) {
		System.out.println(e);
	}
	return 0;
}

private void setDataFromAccountHolderDetails() {
	try {
		ps = con.prepareStatement("select * from account_holder_details where account_number=?");
		ps.setLong(1, beanClass.getAccountNumber());
		rs = ps.executeQuery();
		if(rs.next()) {
		beanClass.setHolderName(rs.getString("holder_name"));
		beanClass.setHolderNumber(rs.getString("holder_number"));
		beanClass.setAadharNumber(rs.getString("aadhar_number"));
		setDataFromAmountDetails();
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
}

public void setDataFromAmountDetails() {
	try {
		ps = con.prepareStatement("select * from amount_details where account_number=?");
		ps.setLong(1, beanClass.getAccountNumber());
		rs = ps.executeQuery();
		if(rs.next()) {
			beanClass.setAmount(rs.getLong("amount"));
			System.out.println(beanClass.getAccountNumber());
			System.out.println(beanClass.getAadharNumber());
			System.out.println(beanClass.getAmount());
			System.out.println(beanClass.getHolderName());
			System.out.println(beanClass.getHolderNumber());
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	finally {
		setDataFromRandomMessages();
	}
}

public boolean depositamount(long depositedAmount) {
	try {
		System.out.println("Deposited Amount: "+depositedAmount);
		ps = con.prepareStatement("update amount_details set amount=? where account_number=?");
		ps.setLong(1, depositedAmount);
		ps.setLong(2, beanClass.getAccountNumber());
		int row = ps.executeUpdate();
		if(row==1) {
			return true;
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return false;
}

public void setDataFromRandomMessages() {
	try {
		con = DriverManager.getConnection(db_url, user, pass);
		ps = con.prepareStatement("SELECT * FROM random_messages ORDER BY RAND() LIMIT 1");
		rs = ps.executeQuery();
		if(rs.next()) {
			beanClass.setMessage(rs.getString("message"));
			System.out.println(beanClass.getMessage());
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
}

public boolean transferOperation(long transferAccountNumber, long transferAmount) {
	try {
		ps = con.prepareStatement("update amount_details set amount=? where account_number=?");
		ps.setLong(1, transferAmount);
		ps.setLong(2, transferAccountNumber);
		int row = ps.executeUpdate();
		if(row==1) {
			return true;
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return false;	
}

public Long getAmountByAccountNumber(long transferAccountNumber) {
	try {
		con = DriverManager.getConnection(db_url, user, pass);
		ps = con.prepareStatement("SELECT amount FROM amount_details where account_number=?");
		ps.setLong(1, transferAccountNumber);
		rs = ps.executeQuery();
		if(rs.next()) {
			return rs.getLong("amount");
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
}
}

